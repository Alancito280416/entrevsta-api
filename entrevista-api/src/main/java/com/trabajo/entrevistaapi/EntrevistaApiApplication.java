package com.trabajo.entrevistaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntrevistaApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(EntrevistaApiApplication.class, args);
    }

}
