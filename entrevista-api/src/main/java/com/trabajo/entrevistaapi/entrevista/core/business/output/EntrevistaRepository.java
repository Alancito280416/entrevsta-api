package com.trabajo.entrevistaapi.entrevista.core.business.output;

import com.trabajo.entrevistaapi.entrevista.core.entity.*;

import java.util.List;

public interface EntrevistaRepository {
    void saveCliente(Cliente cliente);

    void saveListaCompra(ListaCompra listaCompra);

    void saveProducto(Producto producto);

    void saveListaDetallada(List<ListaCompraDetalle> listaCompraDetalle);
    List<ListaConsultar> getListaConsultaar(Integer idCliente);
    List<Lista> getLista(Integer idLista);
}
