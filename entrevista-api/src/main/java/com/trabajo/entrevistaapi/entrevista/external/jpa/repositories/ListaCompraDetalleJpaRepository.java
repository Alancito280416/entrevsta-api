package com.trabajo.entrevistaapi.entrevista.external.jpa.repositories;

import com.trabajo.entrevistaapi.entrevista.external.jpa.model.ListaCompraDetalleJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ListaCompraDetalleJpaRepository extends JpaRepository<ListaCompraDetalleJpa,Integer> {
}
