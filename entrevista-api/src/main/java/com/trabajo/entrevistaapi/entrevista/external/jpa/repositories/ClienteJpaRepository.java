package com.trabajo.entrevistaapi.entrevista.external.jpa.repositories;

import com.trabajo.entrevistaapi.entrevista.external.jpa.model.ClienteJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteJpaRepository extends JpaRepository<ClienteJpa,Integer> {
}
