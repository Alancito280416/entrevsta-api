package com.trabajo.entrevistaapi.entrevista.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trabajo.entrevistaapi.entrevista.core.entity.ListaCompra;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(name = "ListaCompra",description = "Lista de compra")
public class ListaCompraSaveDto {


    @JsonProperty
    @Schema(description = "identificador del cliente")
    private Integer idCliente;

    @JsonProperty
    @Schema(description = "Nombre de la lista")
    private String nombreLista;

    public ListaCompra toEntity(){
        return ListaCompra.builder()
                .idCliente(idCliente)
                .nombreLista(nombreLista)
                .build();
    }

}
