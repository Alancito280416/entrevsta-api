package com.trabajo.entrevistaapi.entrevista.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trabajo.entrevistaapi.entrevista.core.entity.ListaConsultar;
import lombok.*;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(name = "ListaConsultar",description = "Lista detallada")
public class ListaConsultarDto
{
    @JsonProperty
    @Schema(description = "Identificador del cliente")
    private Integer idCliente;


    @JsonProperty
    @Schema(description = "Nombre del cliente")
    private String nombreCliente;


    @JsonProperty
    @Schema(description = "Nombre de la lista")
    private String nombreLista;

    @JsonProperty
    @Schema(description = "Informacion de la Lista")
    private List<ListaDto> listaDtoList;

    public static ListaConsultarDto fromEntity(ListaConsultar listaConsultar){
        return ListaConsultarDto.builder()
                .idCliente(listaConsultar.getIdCLiente())
                .nombreCliente(listaConsultar.getNombreCliente())
                .nombreLista(listaConsultar.getNombreLista())
                .listaDtoList(listaConsultar.getListaList().stream().map(ListaDto::fromEntity).collect(Collectors.toList()))
                .build();
    }

}
