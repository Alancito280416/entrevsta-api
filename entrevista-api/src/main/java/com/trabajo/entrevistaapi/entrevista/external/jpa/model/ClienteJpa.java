package com.trabajo.entrevistaapi.entrevista.external.jpa.model;


import com.trabajo.entrevistaapi.entrevista.core.entity.Cliente;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "et01_clientes")
public class ClienteJpa {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "et01_clientes_id_cliente_seq")
    @SequenceGenerator(name = "et01_clientes_id_cliente_seq", sequenceName = "et01_clientes_id_cliente_seq", allocationSize = 1)
    @Column(name = "id_cliente")
    private Integer idCliente;

    @Column(name = "tx_nombre")
    private String nombreCliente;

    @Column(name = "st_activo")
    private Boolean activo;

    public static ClienteJpa fromEntity(Cliente cliente){
        return ClienteJpa.builder()
                .idCliente(cliente.getIdCliente())
                .nombreCliente(cliente.getNombreCliente())
                .activo(cliente.getActivo())
                .build();
    }

}
