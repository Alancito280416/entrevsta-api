package com.trabajo.entrevistaapi.entrevista.external.rest.controller;

import com.trabajo.entrevistaapi.entrevista.core.business.input.EntrevistaService;
import com.trabajo.entrevistaapi.entrevista.external.rest.dto.*;
import com.trabajo.entrevistaapi.util.error.ErrorResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.PathParam;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/entrevista/trabajo")
public class EntrevistaController {


    @Autowired
    EntrevistaService entrevistaService;

    @PostMapping(value = "/save-cliente", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponse(responseCode = "200",description = "Respuesta exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @ApiResponse(responseCode = "400",description = "Error en la peticion", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    @Operation(operationId = "saveCliente", summary = "Enpoint que salva un cliente", description = "Endpoint que implementa el negocio de crear un cliente")
    public ResponseEntity<?> saveCliente(@RequestBody ClienteDto clienteDto){
        var result=entrevistaService.createCliente(clienteDto.toEntity());
        if(result.isRight()){
            return ResponseEntity.ok(result.get());
        }else {
            return ResponseEntity.status(400).body("Error al guardar el cliente");
        }
    }

    @PostMapping(value = "/save-lista-compra", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponse(responseCode = "200",description = "Respuesta exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @ApiResponse(responseCode = "400",description = "Error en la peticion", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    @Operation(operationId = "saveListaCompra", summary = "Enpoint que salva un cliente", description = "Endpoint que implementa el negocio de crear un cliente")
    public ResponseEntity<?> saveListaCompra(@RequestBody ListaCompraSaveDto listaCompraSaveDto){
        var result=entrevistaService.createListaCompra(listaCompraSaveDto.toEntity());
        if(result.isRight()){
            return ResponseEntity.ok(result.get());
        }else {
            return ResponseEntity.status(400).body("Error al guardar lista");
        }
    }

    @PostMapping(value = "/save-producto", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponse(responseCode = "200",description = "Respuesta exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @ApiResponse(responseCode = "400",description = "Error en la peticion", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    @Operation(operationId = "saveProducto", summary = "Enpoint que salva un cliente", description = "Endpoint que implementa el negocio de crear un cliente")
    public ResponseEntity<?> saveProducto(@RequestBody ProductoSaveDto productoSaveDto){
        var result=entrevistaService.createProducto(productoSaveDto.toEntity());
        if(result.isRight()){
            return ResponseEntity.ok(result.get());
        }else {
            return ResponseEntity.status(400).body("Error al guardar el producto");
        }
    }

    @PostMapping(value = "/save-lista-detallada", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponse(responseCode = "200",description = "Respuesta exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @ApiResponse(responseCode = "400",description = "Error en la peticion", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    @Operation(operationId = "saveListaDetallada", summary = "Enpoint que salva un cliente", description = "Endpoint que implementa el negocio de crear un cliente")
    public ResponseEntity<?> saveListaDetallada(@RequestBody List<ListaDetalladaDto> listaCompraSaveDtoList){
        var result=entrevistaService.createListaDetallada(listaCompraSaveDtoList.stream().map(ListaDetalladaDto::toEntity).collect(Collectors.toList()));
        if(result.isRight()){

            return ResponseEntity.ok(result.get());
        }else {
            return ResponseEntity.status(400).body("Error al guardar la lista");
        }
    }

    @GetMapping(value = "/consultar-lista-detallada{idCliente}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponse(responseCode = "200",description = "Respuesta exitosa", content = @Content(schema = @Schema(type = "array",implementation = ListaConsultarDto.class)))
    @ApiResponse(responseCode = "400",description = "Error en la peticion", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    @Operation(operationId = "getListaDetallada", summary = "Enpoint que obtiene los datos de una lista de un cliente", description = "Endpoint que busca la informacion de una lista de un cliente")
    public ResponseEntity<?> getListaDetallada(@PathParam("idCliente") Integer idCliente){
        var result=entrevistaService.getListaDetallada(idCliente);

        if(result.isRight()){
            List<ListaConsultarDto> listaConsultarDto=result.get().stream().map(ListaConsultarDto::fromEntity).collect(Collectors.toList());
            return ResponseEntity.ok(listaConsultarDto);
        }else {
            return ResponseEntity.status(400).body("Error al guardar la lista");
        }
    }


}
