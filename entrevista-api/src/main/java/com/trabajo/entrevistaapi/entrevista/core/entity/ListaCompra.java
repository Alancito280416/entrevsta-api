package com.trabajo.entrevistaapi.entrevista.core.entity;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ListaCompra {

    private Integer idLista;

    private Integer idCliente;

    private String nombreLista;

    private LocalDateTime fechaRegistro;

    private LocalDateTime fechaUltimaActualizacion;

    private Boolean activo;

}
