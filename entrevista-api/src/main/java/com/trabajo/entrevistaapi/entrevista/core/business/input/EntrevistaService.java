package com.trabajo.entrevistaapi.entrevista.core.business.input;

import com.trabajo.entrevistaapi.entrevista.core.entity.*;
import com.trabajo.entrevistaapi.util.error.ErrorCode;
import io.vavr.control.Either;

import java.util.List;

public interface EntrevistaService {
    Either<ErrorCode,Boolean> createCliente(Cliente cliente);

    Either<ErrorCode,Boolean> createListaCompra(ListaCompra listaCompra);

    Either<ErrorCode,Boolean> createProducto(Producto producto);

    Either<ErrorCode,Boolean> createListaDetallada(List<ListaCompraDetalle> listaCompraDetalle);
    Either<ErrorCode, List<ListaConsultar>> getListaDetallada(Integer idCliente);

}
