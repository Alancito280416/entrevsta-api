package com.trabajo.entrevistaapi.entrevista.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trabajo.entrevistaapi.entrevista.core.entity.ListaCompraDetalle;
import lombok.*;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(name = "ListaDetallada",description = "Modelo utilizado para guardar una lista detallada")
public class ListaDetalladaDto {

    @JsonProperty
    @Schema(description = "Identificador de la lista")
    private Integer idLista;
    @JsonProperty
    @Schema(description = "Identificador del prodcuto")
    private Integer idProducto;

    @JsonProperty
    @Schema(description = "Identificador de la cantidad")
    private Integer cantidad;

    public ListaCompraDetalle toEntity(){
        return ListaCompraDetalle.builder()
                .idLista(idLista)
                .idProducto(idProducto)
                .nuCantidad(cantidad)
                .build();
    }
}
