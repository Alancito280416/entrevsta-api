package com.trabajo.entrevistaapi.entrevista.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trabajo.entrevistaapi.entrevista.core.entity.Cliente;
import lombok.*;
import org.eclipse.microprofile.openapi.annotations.media.Schema;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(name = "Cliente",description = "Modelo utilizado para guardar un cliente")
public class ClienteDto {


    @JsonProperty
    @Schema(description = "Nombre del cliente")
    private String nombreCliente;

    public Cliente toEntity(){
        return Cliente.builder()
                .nombreCliente(nombreCliente)
                .build();
    }

}
