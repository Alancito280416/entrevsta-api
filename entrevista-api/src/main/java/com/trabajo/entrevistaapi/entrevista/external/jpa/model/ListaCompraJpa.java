package com.trabajo.entrevistaapi.entrevista.external.jpa.model;

import com.trabajo.entrevistaapi.entrevista.core.entity.ListaCompra;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "et02_lista_compra")
public class ListaCompraJpa {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "et02_lista_compra_id_lista_seq")
    @SequenceGenerator(name = "et02_lista_compra_id_lista_seq", sequenceName = "et02_lista_compra_id_lista_seq", allocationSize = 1)
    @Column(name = "id_lista")
    private Integer idLista;

    @Column(name = "fk_id_cliente")
    private Integer idCliente;

    @Column(name = "tx_nombre")
    private String nombreLista;

    @Column(name = "fh_fecha_registro")
    private LocalDateTime fechaRegistro;

    @Column(name = "fh_ultima_actualizacion")
    private LocalDateTime fechaUltimaActualizacion;

    @Column(name = "st_activo")
    private Boolean activo;

    public static ListaCompraJpa fromEntity(ListaCompra listaCompra){
        return ListaCompraJpa.builder()
                .idLista(listaCompra.getIdLista())
                .idCliente(listaCompra.getIdCliente())
                .nombreLista(listaCompra.getNombreLista())
                .fechaRegistro(listaCompra.getFechaRegistro())
                .fechaUltimaActualizacion(listaCompra.getFechaUltimaActualizacion())
                .activo(listaCompra.getActivo())
                .build();

    }
}
