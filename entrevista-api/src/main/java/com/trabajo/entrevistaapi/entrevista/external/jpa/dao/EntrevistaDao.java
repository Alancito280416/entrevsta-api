package com.trabajo.entrevistaapi.entrevista.external.jpa.dao;

import com.trabajo.entrevistaapi.entrevista.core.business.output.EntrevistaRepository;
import com.trabajo.entrevistaapi.entrevista.core.entity.*;
import com.trabajo.entrevistaapi.entrevista.external.jpa.model.ClienteJpa;
import com.trabajo.entrevistaapi.entrevista.external.jpa.model.ListaCompraDetalleJpa;
import com.trabajo.entrevistaapi.entrevista.external.jpa.model.ListaCompraJpa;
import com.trabajo.entrevistaapi.entrevista.external.jpa.model.ProductoJpa;
import com.trabajo.entrevistaapi.entrevista.external.jpa.repositories.ClienteJpaRepository;
import com.trabajo.entrevistaapi.entrevista.external.jpa.repositories.ListaCompraDetalleJpaRepository;
import com.trabajo.entrevistaapi.entrevista.external.jpa.repositories.ListaCompraJpaRepository;
import com.trabajo.entrevistaapi.entrevista.external.jpa.repositories.ProductoJpaRepository;
import com.trabajo.entrevistaapi.util.enums.Numbers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class EntrevistaDao implements EntrevistaRepository {

    @PersistenceContext
    EntityManager entityManager;


    @Autowired
    ClienteJpaRepository clienteJpaRepository;

    @Autowired
    ListaCompraJpaRepository listaCompraJpaRepository;

    @Autowired
    ProductoJpaRepository productoJpaRepository;

    @Autowired
    ListaCompraDetalleJpaRepository listaCompraDetalleJpaRepository;


    private static final String QUERY_FIND_LISTA_DETALLE="select et01.id_cliente, " +
            "       et01.tx_nombre as nombre_cliente, " +
            "       et02.tx_nombre as nombre_lista, " +
            "       et02.id_lista  " +
            "from et01_clientes et01 " +
            "join et02_lista_compra et02 on et01.id_cliente=et02.fk_id_cliente " +
            "where et01.id_cliente =:idCliente ";
    private static final String PARAM_ID_CLIENTE="idCliente";

    private static final String QUERY_FIND_LISTA_CANTIDAD=
            "select et03.id_producto, " +
                    "      et03.tx_clave, " +
                    "      et04.nu_cantidad  " +
                    "from et04_lista_compra_detalle et04  " +
                    "join et03_productos et03 on et04.fk_id_producto  =et03.id_producto " +
                    "where et04.fk_id_lista =:idLista ";
    private static final String PARAM_ID_LISTA="idLista";
    @Override
    public void saveCliente(Cliente cliente) {
        clienteJpaRepository.saveAndFlush(ClienteJpa.fromEntity(cliente));

    }

    @Override
    public void saveListaCompra(ListaCompra listaCompra) {
        listaCompraJpaRepository.saveAndFlush(ListaCompraJpa.fromEntity(listaCompra));
    }

    @Override
    public void saveProducto(Producto producto) {
        productoJpaRepository.saveAndFlush(ProductoJpa.fromEntity(producto));
    }

    @Override
    public void saveListaDetallada(List<ListaCompraDetalle> listaCompraDetalle) {
        listaCompraDetalleJpaRepository.saveAll(listaCompraDetalle.stream().map(ListaCompraDetalleJpa::fromEntity).collect(Collectors.toList()));
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ListaConsultar> getListaConsultaar(Integer idCliente) {
        Stream<Object[]> stream=entityManager.createNativeQuery(QUERY_FIND_LISTA_DETALLE)
                .setParameter(PARAM_ID_CLIENTE,idCliente)
                .getResultStream();
        return stream.map(row-> ListaConsultar.builder()
                .idCLiente((Integer) row[Numbers.CERO.getInteger()])
                .nombreCliente((String) row[Numbers.UNO.getInteger()])
                .nombreLista((String) row[Numbers.DOS.getInteger()])
                .idLista((Integer) row[Numbers.TRES.getInteger()])
                .build()).collect(Collectors.toList());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Lista> getLista(Integer idLista) {
        Stream<Object[]> stream=entityManager.createNativeQuery(QUERY_FIND_LISTA_CANTIDAD)
                .setParameter(PARAM_ID_LISTA,idLista)
                .getResultStream();
        return stream.map(row-> Lista.builder()
                .idProducto((Integer) row[Numbers.CERO.getInteger()])
                .claveProduto((String) row[Numbers.UNO.getInteger()])
                .cantidadProducto((Integer) row[Numbers.DOS.getInteger()])
                .build()).collect(Collectors.toList());
    }
}
