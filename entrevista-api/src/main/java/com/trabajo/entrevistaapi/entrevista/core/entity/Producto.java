package com.trabajo.entrevistaapi.entrevista.core.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Producto {
    private Integer idProducto;
    private String clave;
    private String descripcion;
    private Boolean activo;


}
