package com.trabajo.entrevistaapi.entrevista.external.jpa.repositories;

import com.trabajo.entrevistaapi.entrevista.external.jpa.model.ListaCompraJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ListaCompraJpaRepository extends JpaRepository<ListaCompraJpa, Integer> {
}
