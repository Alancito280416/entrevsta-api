package com.trabajo.entrevistaapi.entrevista.core.business.implementation;

import com.trabajo.entrevistaapi.entrevista.core.business.input.EntrevistaService;
import com.trabajo.entrevistaapi.entrevista.core.business.output.EntrevistaRepository;
import com.trabajo.entrevistaapi.entrevista.core.entity.*;
import com.trabajo.entrevistaapi.util.error.ErrorCode;
import io.vavr.control.Either;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class EntrevistaBs implements EntrevistaService {

    @Autowired
    EntrevistaRepository entrevistaRepository;
    @Override
    @Transactional
    public Either<ErrorCode, Boolean> createCliente(Cliente cliente) {
        Cliente clienteSave= Cliente.builder()
                .nombreCliente(cliente.getNombreCliente())
                .activo(true)
                .build();
        entrevistaRepository.saveCliente(clienteSave);

        return Either.right(true);
    }

    @Override
    @Transactional
    public Either<ErrorCode, Boolean> createListaCompra(ListaCompra listaCompra) {
        ListaCompra listaCompraSave= ListaCompra.builder()
                .idCliente(listaCompra.getIdCliente())
                .nombreLista(listaCompra.getNombreLista())
                .fechaRegistro(LocalDateTime.now())
                .fechaUltimaActualizacion(LocalDateTime.now())
                .activo(true)
                .build();
        entrevistaRepository.saveListaCompra(listaCompraSave);
        return Either.right(true);
    }

    @Override
    @Transactional
    public Either<ErrorCode, Boolean> createProducto(Producto producto) {
        Producto productoSave= Producto.builder()
                .clave(producto.getClave())
                .descripcion(producto.getDescripcion())
                .activo(true)
                .build();
        entrevistaRepository.saveProducto(productoSave);
        return Either.right(true);
    }

    @Override
    public Either<ErrorCode, Boolean> createListaDetallada(List<ListaCompraDetalle> listaCompraDetalleList) {

        entrevistaRepository.saveListaDetallada(listaCompraDetalleList);
        return Either.right(true);
    }

    @Override
    public Either<ErrorCode, List<ListaConsultar>>getListaDetallada(Integer idCliente) {
       List<ListaConsultar> listaConsultaar=entrevistaRepository.getListaConsultaar(idCliente);
       listaConsultaar.forEach(listaConsultar ->{
           List<Lista> listaList=entrevistaRepository.getLista(listaConsultar.getIdLista());
                   listaConsultar.setListaList(listaList);
               }
               );
        return Either.right(listaConsultaar);
    }

}
