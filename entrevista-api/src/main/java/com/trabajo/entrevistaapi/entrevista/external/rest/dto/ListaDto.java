package com.trabajo.entrevistaapi.entrevista.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trabajo.entrevistaapi.entrevista.core.entity.Lista;
import lombok.*;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(name = "Lista",description = "Lista de un cliente")
public class ListaDto {
    @JsonProperty
    @Schema(description = "Identificador del prodcuto")
    private Integer idProducto;

    @JsonProperty
    @Schema(description = "Identificador de la clave del producto")
    private String claveProduto;

    @JsonProperty
    @Schema(description = "Identificador de la cantidad del producto")
    private Integer cantidadProducto;

    public static ListaDto fromEntity(Lista lista){
        return ListaDto.builder()
                .idProducto(lista.getIdProducto())
                .claveProduto(lista.getClaveProduto())
                .cantidadProducto(lista.getCantidadProducto())
                .build();
    }
}
