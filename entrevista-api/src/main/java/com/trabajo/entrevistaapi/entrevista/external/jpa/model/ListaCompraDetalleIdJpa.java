package com.trabajo.entrevistaapi.entrevista.external.jpa.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Embeddable
public class ListaCompraDetalleIdJpa implements Serializable {
    @Column(name = "fk_id_lista", nullable = false)
    private Integer idLista;
    @Column(name = "fk_id_producto", nullable = false)
    private Integer idProducto;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListaCompraDetalleIdJpa that = (ListaCompraDetalleIdJpa) o;
        return Objects.equals(idLista, that.idLista) && Objects.equals(idProducto, that.idProducto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idLista, idProducto);
    }
}
