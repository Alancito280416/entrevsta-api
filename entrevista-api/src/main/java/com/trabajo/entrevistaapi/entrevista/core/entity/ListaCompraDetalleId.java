package com.trabajo.entrevistaapi.entrevista.core.entity;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ListaCompraDetalleId {
    private Integer idLista;
    private Integer idProducto;
}
