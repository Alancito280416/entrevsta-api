package com.trabajo.entrevistaapi.entrevista.external.jpa.model;

import com.trabajo.entrevistaapi.entrevista.core.entity.ListaCompraDetalle;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "et04_lista_compra_detalle")
public class ListaCompraDetalleJpa {

    @EmbeddedId
    private ListaCompraDetalleIdJpa id;

    @Column(name = "fk_id_lista", insertable = false, updatable = false)
    private Integer idLista;
    @Column(name = "fk_id_producto", insertable = false, updatable = false)
    private Integer idProducto;

    @Column(name = "nu_cantidad")
    private Integer nuCantidad;

    public static ListaCompraDetalleJpa fromEntity(ListaCompraDetalle listaCompraDetalle){
       return ListaCompraDetalleJpa.builder()
               .id(ListaCompraDetalleIdJpa.builder().idLista(listaCompraDetalle.getIdLista()).idProducto(listaCompraDetalle.getIdProducto()).build())
               .idLista(listaCompraDetalle.getIdLista())
               .idProducto(listaCompraDetalle.getIdProducto())
               .nuCantidad(listaCompraDetalle.getNuCantidad())
               .build();
    }
}
