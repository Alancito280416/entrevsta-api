package com.trabajo.entrevistaapi.entrevista.external.jpa.repositories;

import com.trabajo.entrevistaapi.entrevista.external.jpa.model.ProductoJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductoJpaRepository extends JpaRepository<ProductoJpa,Integer> {
}
