package com.trabajo.entrevistaapi.entrevista.core.entity;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ListaConsultar {

    private Integer idCLiente;
    private String nombreCliente;
    private String nombreLista;
    private Integer idLista;
    private List<Lista> listaList;

}
