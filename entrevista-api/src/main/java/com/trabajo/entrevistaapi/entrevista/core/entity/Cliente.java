package com.trabajo.entrevistaapi.entrevista.core.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Cliente {
    private Integer idCliente;
    private String nombreCliente;
    private Boolean activo;

}
