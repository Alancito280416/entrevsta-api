package com.trabajo.entrevistaapi.entrevista.core.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ListaCompraDetalle {
    private ListaCompraDetalleId listaCompraDetalleId;
    private Integer idLista;
    private Integer idProducto;
    private Integer nuCantidad;
}
