package com.trabajo.entrevistaapi.entrevista.core.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Lista {
    private Integer idProducto;
    private String claveProduto;
    private Integer cantidadProducto;
}
