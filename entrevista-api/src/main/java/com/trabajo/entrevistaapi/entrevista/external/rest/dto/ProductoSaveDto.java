package com.trabajo.entrevistaapi.entrevista.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trabajo.entrevistaapi.entrevista.core.entity.Producto;
import lombok.*;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(name = "ProductoSave",description = "Modelo utilizado para guardar un producto")
public class ProductoSaveDto {

    @JsonProperty
    @Schema(description = "clave del producto")
    private String clave;

    @JsonProperty
    @Schema(description = "descripcion del producto")
    private String descripcion;

    public Producto toEntity(){
        return Producto.builder()
                .clave(clave)
                .descripcion(descripcion)
                .build();
    }
}
