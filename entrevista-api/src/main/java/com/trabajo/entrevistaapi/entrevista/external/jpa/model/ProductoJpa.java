package com.trabajo.entrevistaapi.entrevista.external.jpa.model;


import com.trabajo.entrevistaapi.entrevista.core.entity.Producto;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "et03_productos")
public class ProductoJpa {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "et03_productos_id_producto_seq")
    @SequenceGenerator(name = "et03_productos_id_producto_seq", sequenceName = "et03_productos_id_producto_seq", allocationSize = 1)
    @Column(name = "id_producto")
    private Integer idProducto;

    @Column(name = "tx_clave")
    private String clave;

    @Column(name = "tx_descripcion")
    private String descripcion;

    @Column(name = "st_activo")
    private Boolean activo;

    public static ProductoJpa fromEntity(Producto producto){
        return ProductoJpa.builder()
                .idProducto(producto.getIdProducto())
                .clave(producto.getClave())
                .descripcion(producto.getDescripcion())
                .activo(producto.getActivo())
                .build();
    }
}
