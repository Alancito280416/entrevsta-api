package com.trabajo.entrevistaapi.util.error;


public enum ErrorCodesEnum implements ErrorCode {
    /**
     * Codigos de error de reglas de negocio de sistema
     */


    RS_ERROR("Error inesperado"),
    /**
     * Errores NO asociados a una regla de negocio
     */
    RS_INVALID_LINK("Liga no vigente"),
    BAD_REQUEST("Error en la petición"),
    NOT_FOUND("Recurso no encontrado"),
    CAPA_PERSISTENCIA("Error en la capa de persistencia"),
    /**
     * Codigos de error de reglas de negocio de negocio
     */
    ;

    private String detail;

    ErrorCodesEnum(String detail) {
        this.detail = detail;
    }


    @Override
    public String getName() {
        return name();
    }

    @Override
    public String getDetail() {
        return this.detail;
    }
}
