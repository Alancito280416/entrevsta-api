package com.trabajo.entrevistaapi.util.error;

public interface ErrorCode {
    String getName();

    String getDetail();
}
