package com.trabajo.entrevistaapi.util.error;

public enum ErrorType {
    FIELD,
    REQUEST
}
