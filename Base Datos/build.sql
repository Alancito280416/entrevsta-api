SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname='entrevista-trupper';
DROP DATABASE IF EXISTS "entrevista-trupper";
CREATE DATABASE "entrevista-trupper";


BEGIN;
\c entrevista-trupper
\i tablas_entrevista.sql

COMMIT;
