CREATE TABLE et01_clientes(
id_cliente integer NOT NULL,
tx_nombre text NOT NULL,
st_activo boolean
);

CREATE SEQUENCE et01_clientes_id_cliente_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    


ALTER SEQUENCE et01_clientes_id_cliente_seq OWNED BY et01_clientes.id_cliente;
ALTER TABLE ONLY et01_clientes ALTER COLUMN id_cliente SET DEFAULT nextval('et01_clientes_id_cliente_seq'::regclass);
ALTER TABLE ONLY et01_clientes ADD CONSTRAINT et01_clientes_pkey PRIMARY KEY (id_cliente);

CREATE TABLE et02_lista_compra(
id_lista integer NOT NULL,
fk_id_cliente integer NOT NULL,
tx_nombre text NOT NULL,
fh_fecha_registro timestamp,
fh_ultima_actualizacion timestamp,
st_activo boolean 
);

CREATE SEQUENCE et02_lista_compra_id_lista_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    
    
    
ALTER SEQUENCE et02_lista_compra_id_lista_seq OWNED BY et02_lista_compra.id_lista;
ALTER TABLE ONLY et02_lista_compra ALTER COLUMN id_lista SET DEFAULT nextval('et02_lista_compra_id_lista_seq'::regclass);
ALTER TABLE ONLY et02_lista_compra ADD CONSTRAINT et02_lista_compra_pkey PRIMARY KEY (id_lista);
ALTER TABLE et02_lista_compra ADD CONSTRAINT FKet02_usua5774440 FOREIGN KEY (fk_id_cliente) REFERENCES et01_clientes(id_cliente);


CREATE TABLE et03_productos(
id_producto integer NOT NULL,
tx_clave text, 
tx_descripcion text,
st_activo boolean 
);

CREATE SEQUENCE et03_productos_id_producto_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    
    
    
ALTER SEQUENCE et03_productos_id_producto_seq OWNED BY et03_productos.id_producto;
ALTER TABLE ONLY et03_productos ALTER COLUMN id_producto SET DEFAULT nextval('et03_productos_id_producto_seq'::regclass);
ALTER TABLE ONLY et03_productos ADD CONSTRAINT et03_productos_pkey PRIMARY KEY (id_producto);

CREATE TABLE et04_lista_compra_detalle(
fk_id_lista integer NOT NULL,
fk_id_producto integer NOT NULL,
nu_cantidad integer
);

ALTER TABLE ONLY et04_lista_compra_detalle ADD CONSTRAINT et04_lista_compra_detalle_pkey PRIMARY KEY (fk_id_lista, fk_id_producto);
ALTER TABLE ONLY et04_lista_compra_detalle ADD CONSTRAINT fket04_perm151282 FOREIGN KEY (fk_id_lista) REFERENCES et02_lista_compra(id_lista);
ALTER TABLE ONLY et04_lista_compra_detalle ADD CONSTRAINT fket04_perm501867 FOREIGN KEY (fk_id_producto) REFERENCES et03_productos(id_producto);
